package com.example.math_game

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [welcome.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [welcome.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class welcome : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null
    internal lateinit var sumButon : Button
    internal lateinit var subButon : Button
    internal lateinit var multButon : Button
    internal lateinit var divButon : Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        sumButon = view.findViewById(R.id.button)
        subButon = view.findViewById(R.id.button2)
        multButon = view.findViewById(R.id.button3)
        divButon = view.findViewById(R.id.button4)
        sumButon.setOnClickListener { goToMainGameSum() }
        subButon.setOnClickListener { goToMainGameSub() }
        multButon.setOnClickListener { goToMainGameMult() }
        divButon.setOnClickListener { goToMainGameDiv() }
    }

    fun goToMainGameSum(){
        val action = welcomeDirections.actionMainGame("+")
        view?.findNavController()?.navigate(action)
    }

    fun goToMainGameSub(){
        val action = welcomeDirections.actionMainGame("-")
        view?.findNavController()?.navigate(action)
    }

    fun goToMainGameDiv(){
        val action = welcomeDirections.actionMainGame("/")
        view?.findNavController()?.navigate(action)
    }

    fun goToMainGameMult(){
        val action = welcomeDirections.actionMainGame("x")
        view?.findNavController()?.navigate(action)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
