package com.example.math_game

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver

import kotlin.random.Random


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [main_game.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [main_game.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class main_game : Fragment() {

    // TODO: Rename and change types of parameters

    val args: main_gameArgs by navArgs()

    internal lateinit var sign: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeleftTextView: TextView
    internal lateinit var sendButton: Button
    internal lateinit var first: TextView
    internal lateinit var second: TextView
    internal lateinit var roundtxt: TextView
    internal lateinit var editText2: TextView

    @State //todas las variables que quieremos que se guarde en el state
    var score = 0

    @State
    var timeleft = 10

    @State
    var round = 1


    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer


    var num1 = 0
    var num2 = 0

    var signtxt2 = ""

    private var listener: OnFragmentInteractionListener? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        StateSaver.restoreInstanceState(this, savedInstanceState)
        roundtxt = view.findViewById(R.id.textView2)
        gameScoreTextView = view.findViewById(R.id.textView3)
        sendButton = view.findViewById(R.id.button5)
        timeleftTextView = view.findViewById(R.id.textView4)
        editText2 = view.findViewById(R.id.editText2)

        first = view.findViewById(R.id.textView5)
        second = view.findViewById(R.id.textView6)
        gameScoreTextView.text = getString(R.string.score, score)

        sign = view.findViewById(R.id.textView7)
        val signtxt = args.sign
        sign.text = getString(R.string.sign, signtxt)
        signtxt2 = getString(R.string.sign, signtxt)
        roundtxt.text = getString(R.string.round, round)
        sendButton.setOnClickListener { doOperation() }

        startGame()



    }


    fun startGame(){
        if (round <= 5){
            roundtxt.text = getString(R.string.round, round)
            timeleft = 10
            num1 = Random.nextInt(1, 10)
            num2 = Random.nextInt(1, 10)
            first.text = getString(R.string.first, num1)
            second.text = getString(R.string.second, num2)

            countDownTimer = object: CountDownTimer(initialCountDown, countDownInterval){
                override fun onFinish() {
                    doOperation()
                }

                override fun onTick(millisUntilFinished: Long) {
                    timeleft = millisUntilFinished.toInt()/1000
                    timeleftTextView.text = getString(R.string.time_left, timeleft)
                }


            }
            countDownTimer.start()


        } else {
            roundtxt.text = getString(R.string.round, round-1)
            Toast.makeText(activity, getString(R.string.score, score), Toast.LENGTH_LONG).show()
            sendButton.isClickable = false
        }

        editText2.text = ""

    }

    fun doOperation(){
        val result = editText2.text.toString().toDouble()
        if(signtxt2 == "+"){
            if(result == (num1 + num2).toDouble()){
                score = score + checkTime()
            }else{
                score = 0
            }
        }else if(signtxt2 == "-"){
            if(result == (num1 - num2).toDouble()){
                score = score + checkTime()
            }else{
                score = 0
            }
        }else if(signtxt2 == "/"){
            if(result == (num1 / num2).toDouble()){
                score = score + checkTime()
            }else{
                score = 0
            }
        }else if(signtxt2 == "x"){
            if(result == (num1 * num2).toDouble()){
                score = score + checkTime()
            }else{
                score = 0
            }
        }

        gameScoreTextView.text = getString(R.string.score, score)
        round = round + 1
        countDownTimer.cancel()
        startGame()
    }

    fun checkTime(): Int {
        if(timeleft >= 8 && timeleft < 10){
            return 100
        }else if(timeleft > 5 ){
            return 50
        }else {
            return 10
        }
    }









    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
        countDownTimer.cancel()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
